#!/usr/bin/env python
# coding: utf-8

# In[1]:


from transformers import BertTokenizer, BertModel
import torch

# Load pre-trained BERT model and tokenizer
model_name = 'sentence-transformers/LaBSE'
tokenizer = BertTokenizer.from_pretrained(model_name)
bert_model = BertModel.from_pretrained(model_name)

if torch.cuda.is_available():
    print("using cuda")
    bert_model = bert_model.to('cuda')
else:
    print("not using cuda")

# In[32]:


import os
from translate.storage.tmx import tmxfile

pairs = []
# structure:
# pairs = [
#     {
#        "sent1": "sentence1",
#        "sent2": "sentence2",
#     }
#]

for root, dirs, filenames in os.walk("maalfrid"):
    if "-" in root:  # in a subfolder with tmx pair files
        folder = root.split("/")[1]
        sourcelang, targetlang = folder.split("-")
        if  (sourcelang, targetlang) == ("nb", "nn"):
            continue

        for filename in filenames:
            with open(root + "/" + filename, "rb") as f:
                tmx_file = tmxfile(f, sourcelang, targetlang)

            for node in tmx_file.unit_iter():
                pairs.append({
                    "sent1": node.source,
                    "sent2": node.target
                })


# In[54]:


embeddings = [] 
# structure:
# [
#     {
#        "embed1": [...],
#        "embed2": [...],
#     }
# ]

def labse_embed(sentence):
    # Tokenize the sentence
    tokens = tokenizer.tokenize(tokenizer.decode(tokenizer.encode(sentence)))
    # Convert tokens to IDs
    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    # Convert input IDs to PyTorch tensor
    input_ids_tensor = torch.tensor([input_ids])

    if torch.cuda.is_available():
        input_ids_tensor = input_ids_tensor.to("cuda")

    # Pass input through BERT model
    with torch.no_grad():
        return bert_model(input_ids_tensor).pooler_output[0] #pooler_output is CLS token (sentence embedding)
from tqdm import tqdm

for pair in tqdm(pairs):
    embed1 = labse_embed(pair["sent1"])
    embed2 = labse_embed(pair["sent2"])
    embeddings.append({
        "embed1": embed1,
        "embed2": embed2,
    })


# In[58]:


torch.save(embeddings, "training_data.pt")


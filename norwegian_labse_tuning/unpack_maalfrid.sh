#!/bin/bash

MALFRID_ZIP="20221223_maalfrid_tmx.tar.gz"

tar -xvf $MALFRID_ZIP
mv tmx maalfrid
cd maalfrid

#rm $MALFRID_ZIP

mkdir en-nb en-nn nb-nn
gunzip *.gz

mv *en-nb* en-nb 2>/dev/null
mv *en-nn* en-nn 2>/dev/null
mv *nb-nn* nb-nn 2>/dev/null 

import sys
import torch
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm, trange
import numpy as np
from sentence_transformers import SentenceTransformer, InputExample, losses
from scandeval import Benchmarker


def doubleprint(*toprint, file):
        toprint = (str(e) for e in toprint)
        toprint = " ".join(toprint)
        print(toprint)
        print(toprint, file=file)

benchmarker = Benchmarker(trust_remote_code=True)
model, task, language = sys.argv[1:]

benchmarker(model, task=task, language=language)

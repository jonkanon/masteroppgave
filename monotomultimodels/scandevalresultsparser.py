import json
from prettytable import PrettyTable

jsonl_file_path = 'scandeval_benchmark_results.jsonl'
data = []
with open(jsonl_file_path, "r") as f:
    for line in f:
        try:
            data.append(json.loads(line))
        except:
            continue

tests = {}
#print(len(data))
sample = data[3]
#print(sample["model"])
#print(sample["results"]["total"])

for result in data:
    if "da-en" in result["model"]:
        continue
    task_dataset = f"{result['task']}_{result['dataset']}"
    if task_dataset not in tests.keys():
        tests[task_dataset] = {}
    tests[task_dataset][result["model"]] = result["results"]["total"]

def format_metrics(metrics, metricnames):
    formatted_metrics = []

    for metric in metricnames:
        formatted = f"{round(metrics[metric], 2)} +- {round(metrics[f'{metric}_se'],2)}"
        formatted_metrics.append(formatted)

    return formatted_metrics


for task in tests.keys():
    # Create a table
    table = PrettyTable()
    taskname, datasetname = task.split("_")
    table.title = f"Task: {taskname}, Dataset: {datasetname}"
    metricnames = [item for item in list(list(tests[task].values())[0].keys()) if "_se" not in item]
    metric_to_sort_by = metricnames[0]
    table.field_names = ["Model"] + metricnames

    # Sort models based on test MCC
    sorted_models = sorted(tests[task].items(), key=lambda x: x[1][metric_to_sort_by], reverse=True)

    # Populate the table
    for model, metrics in sorted_models:
        formatted_metrics = format_metrics(metrics, metricnames)
        try:
            model = model.split("/")[-1]
        except:
            pass
        table.add_row([model] + formatted_metrics)

    # Print the table
    print(table)

#!/bin/sh
source ../venv/bin/activate
python make_multilingual_nn_10k.py
python make_multilingual_nn_25k.py
python make_multilingual_nn_50k.py
python make_multilingual_nn_100k.py
python make_multilingual_nn_200k.py

import sys
import os
import subprocess

modelsdir="models/variedatamodels"

#models = os.listdir(modelsdir)
task, language = sys.argv[1:]

venvpath = "/cluster/home/jonalan/masteroppgave/scandevalenv/bin/activate"

additional_models = ["models/300kmodels/en-nn"]
#additional_models = ["xlm-roberta-base", "ltg/norbert3-base"]
models = [f.path for f in os.scandir(modelsdir) if f.is_dir() and not "models" in str(f)] + additional_models
print(models)
for model in models:
    command = f"source {venvpath} && python scandeval_single.py {model} {task} {language}"
    subprocess.run(command, shell=True, check=True)

import subprocess
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import os
import datetime
import sys

class MyHandler(FileSystemEventHandler):
    def __init__(self, file_to_run):
        super().__init__()
        self.file_to_run = file_to_run

    def on_modified(self, event):
        #print(f"{event.src_path} has been modified. Running script...")
        os.system("clear")
        time = datetime.datetime.now().strftime("%H:%M:%S")
        print(f"{time}\n")
        subprocess.run(f"source /cluster/home/jonalan/masteroppgave/venv/bin/activate && python {self.file_to_run}", shell=True)

if __name__ == "__main__":
    file_to_run = sys.argv[1]  # Change this to the path of the script you want to monitor
    event_handler = MyHandler(file_to_run)
    observer = Observer()
    observer.schedule(event_handler, ".", recursive=False)  # Monitor only the current directory for changes
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()

